user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log info;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '[$time_local] $status $request: $body_bytes_sent'
            ' $remote_addr "$http_referer" '
            '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;

    keepalive_timeout  65;

    upstream uwsgi {
        server app:5000;
    }

    server {
        listen 443 ssl;

        ssl_certificate /etc/nginx/certs/default.crt;
        ssl_certificate_key /etc/nginx/certs/default.key;

        location /static/ {
            autoindex on;
            alias /static/;
        }

        location /media/ {
            autoindex on;
            alias /media/;
        }

        location / {
            uwsgi_pass uwsgi;
            include uwsgi_params;
        }
    }
}